-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 21, 2020 at 06:57 AM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `restaurant`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_info`
--

CREATE TABLE `admin_info` (
  `id` int(10) NOT NULL,
  `first_name` varchar(15) NOT NULL,
  `last_name` varchar(15) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(30) NOT NULL,
  `contact` int(15) NOT NULL,
  `address` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_info`
--

INSERT INTO `admin_info` (`id`, `first_name`, `last_name`, `email`, `password`, `contact`, `address`) VALUES
(1, 'John', 'Rai', 'John', 'john', 98989898, 'Godawari');

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `id` int(15) NOT NULL,
  `user_name` varchar(25) NOT NULL,
  `user_email` varchar(40) NOT NULL,
  `user_mob` int(15) NOT NULL,
  `user_msg` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`id`, `user_name`, `user_email`, `user_mob`, `user_msg`) VALUES
(1, 'Mike', 'mike@gmail.com', 98989898, '0'),
(2, 'Terry', 'terry@gmail.com', 123456789, 'this is for testing pt two');

-- --------------------------------------------------------

--
-- Table structure for table `food_order`
--

CREATE TABLE `food_order` (
  `address_One` varchar(50) NOT NULL,
  `address_Two` varchar(50) NOT NULL,
  `Chickenpizza` int(5) NOT NULL,
  `Mushroompizza` int(5) NOT NULL,
  `Cheesepizza` int(5) NOT NULL,
  `Chickenchilly` int(5) NOT NULL,
  `Buffchilly` int(5) NOT NULL,
  `Buffchowmein` int(5) NOT NULL,
  `Chickenchowmein` int(5) NOT NULL,
  `Buffmomo` int(5) NOT NULL,
  `Chickenmomo` int(5) NOT NULL,
  `Chickenburger` int(5) NOT NULL,
  `Vegburger` int(5) NOT NULL,
  `Schezwanfriedrice` int(5) NOT NULL,
  `Total` int(10) NOT NULL,
  `Status` int(5) NOT NULL,
  `order_id` int(10) NOT NULL,
  `ordered_by` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `food_order`
--

INSERT INTO `food_order` (`address_One`, `address_Two`, `Chickenpizza`, `Mushroompizza`, `Cheesepizza`, `Chickenchilly`, `Buffchilly`, `Buffchowmein`, `Chickenchowmein`, `Buffmomo`, `Chickenmomo`, `Chickenburger`, `Vegburger`, `Schezwanfriedrice`, `Total`, `Status`, `order_id`, `ordered_by`) VALUES
('ooi', 'oioi', 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 1120, 3, 408037, ''),
('ttttt', 'rrrrrr', 0, 0, 0, 0, 3, 0, 0, 0, 1, 0, 0, 0, 1040, 2, 490887, ''),
('hbkbgu', 'ihbiolb', 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 400, 3, 71112, ''),
('sada', 'dscs', 0, 0, 0, 0, 0, 0, 0, 0, 5, 0, 0, 0, 1000, 2, 552340, ''),
('asasda', 'aadad', 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 400, 2, 241430, ''),
('Patan', 'Patan', 0, 3, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 1140, 2, 145774, ''),
('ktm', 'ktm', 0, 2, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 1300, 2, 167464, ''),
('kt', 'Bansbari', 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 560, 3, 114330, ''),
('Patan', 'gwarko', 2, 0, 0, 0, 3, 0, 0, 0, 0, 0, 2, 0, 1840, 3, 567740, ''),
('Lalitpur', 'Patan', 0, 3, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 1810, 2, 58785, ''),
('Patan', 'Gwarko', 0, 3, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 1290, 2, 117198, ''),
('Chahbil', 'Gaushala', 0, 0, 0, 0, 0, 2, 0, 0, 3, 0, 0, 0, 900, 2, 220438, ''),
('rttrdtrdr', 'vvvvivvv', 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 750, 2, 508578, ''),
('boioi', 'vviiki', 0, 0, 0, 0, 0, 2, 0, 0, 0, 1, 0, 0, 500, 2, 258470, ''),
('vgiivi', 'gviv', 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 400, 2, 541974, ''),
('vvivog', 'tggfg', 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 750, 2, 495022, ''),
('aaaa', 'bbbbb', 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 450, 2, 289525, ''),
('wqwqq', 'wewdw', 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1000, 2, 480124, ''),
('vvvi', 'vtc', 0, 0, 0, 0, 0, 2, 0, 0, 0, 2, 0, 0, 700, 2, 293250, ''),
('fvio', 'gbvobvo', 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 600, 2, 344507, ''),
('giobv', 'iboi0o', 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 600, 2, 390060, ''),
('ttttt', 'rrrrrrrrrr', 0, 3, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 1250, 2, 237176, ''),
('iiiiiii', 'ooooooo', 0, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1440, 2, 290798, ''),
('Patan', 'Gwarko', 0, 3, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 1530, 2, 44453, ''),
('teddd', 'tedtd', 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1250, 2, 231882, ''),
('g]igig', 'gigi', 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1500, 2, 341492, ''),
('Chahbil', 'Gaushala', 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 1400, 1, 326263, '72004'),
('tttttttttttt', 'rrrrrrrrrrrrrrrrrrr', 0, 0, 0, 0, 0, 0, 0, 0, 5, 0, 0, 0, 1000, 1, 559792, '297720'),
('tttttttttttt', 'rrrrrrrrrrrrrrrrrrr', 0, 0, 0, 0, 0, 0, 0, 0, 5, 0, 0, 0, 1000, 1, 559792, '297720'),
('Bansbari', 'Mahrajgunj', 0, 0, 0, 0, 0, 3, 0, 0, 0, 3, 0, 0, 1050, 1, 559377, '304650'),
('Bansbari', 'Mahrajgunj', 0, 0, 0, 0, 0, 3, 0, 0, 0, 3, 0, 0, 1050, 1, 559377, '304650'),
('pppppp', 'lllllllll', 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1150, 1, 530379, '117659'),
('pppppp', 'lllllllll', 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1150, 1, 530379, '117659'),
('Jorpati', 'Boudha', 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 900, 1, 503220, '117659'),
('Jorpati', 'Boudha', 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 900, 1, 503220, '117659'),
('ney york', 'usa', 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 600, 1, 222843, '117659'),
('ney york', 'usa', 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 600, 1, 222843, '117659'),
('Bhaktapur', 'Thimi', 5, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2650, 3, 169949, '70050'),
('Bhaktapur', 'Thimi', 5, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2650, 3, 169949, '70050');

-- --------------------------------------------------------

--
-- Table structure for table `userreg`
--

CREATE TABLE `userreg` (
  `id` int(10) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `mobile_no` varchar(15) NOT NULL,
  `address_district` varchar(20) NOT NULL,
  `address_area` varchar(20) NOT NULL,
  `account_status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userreg`
--

INSERT INTO `userreg` (`id`, `first_name`, `last_name`, `email`, `password`, `mobile_no`, `address_district`, `address_area`, `account_status`) VALUES
(70050, 'Rahul', 'Shah', 'rahul@gmail.com', 'rahul', '123456789', 'Kathmandu', 'Chapali', 'Active'),
(72004, 'Saroj', 'Thapa', 'sad@gmail.com', 'saroj', '9898988988', 'ktm', 'chahbil', 'Active'),
(117659, 'Sajan', 'Pyatha', 'admin', 'sajan', '9860590345', 'Bhaktapur', 'Thimi', 'Active'),
(145533, 'Ritesh', 'Rai', 'ritesh@gmail.com', 'rai', '789456123', 'Boudha', 'Kapan', 'Active'),
(178924, 'Ranjit', 'Magar', 'ranjit@gmail.com', 'ranjit', '198989898', 'Boudha', 'Jorpati', 'Active'),
(202916, 'Ravi', 'Srestha', 'ravi@gmail.co', 'ravi', '123456789', 'Kathmabdu', 'Balaju', 'Active'),
(212017, 'adada', 'sdada', 'admin@gmail.com', 'asd', '213231', 'dadada', 'dadadsa', 'Active'),
(297720, 'Bibek', 'Thapa', 'bibek@gmail.com', 'bibek', '989898989898', 'Kathmandu', 'Balaju', 'Active'),
(304650, 'Sandis', 'Prajapati', 'sajandis.prajapati@gmail.com', 'sandis', '9898989898', 'Kathmandu', 'Bansbari', 'Active'),
(360405, 'Shrawan', 'Thapa', 'thapa@gmail.com', 'thapa', '987456123', 'Kathmandu', 'Bansbari', 'Active'),
(441788, 'Prashant', 'Singh', 'prashant@gmail.com', 'singh', '78945612', 'Gaushala', 'Chahbil', 'Active');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_info`
--
ALTER TABLE `admin_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `userreg`
--
ALTER TABLE `userreg`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_info`
--
ALTER TABLE `admin_info`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `userreg`
--
ALTER TABLE `userreg`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=441789;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
