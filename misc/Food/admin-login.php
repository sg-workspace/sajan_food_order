
<!DOCTYPE html>
<html>
	<head>
		<title>Log In</title>
				  <meta name="viewport" content="width=device-width, initial-scale=1.0">

			 	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
				  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
				  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
				  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
				  <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
				  <link rel="stylesheet" type="text/css" href="CSS/style.css">
	</head>
	<body>
		<div class="image-holder">
			
			
		<div class="container">
			<div class="row">
				<div class="admin-login-container">
					<!-- Material form login -->
					<div class="card">

  						<h5 class="card-header info-color white-text text-center py-4">
   						 <strong>Sign in</strong>
 					    </h5>
 					    <div class="card-body px-lg-5 pt-0">

    					<!-- Form -->
    						<form class="text-center" style="color: #757575;" method="POST">
    							 <!-- Email -->
							      <div class="md-form">
							        <input type="email" id="materialLoginFormEmail" class="form-control" name="email">
							        <label for="materialLoginFormEmail">E-mail</label>
							      </div>
							       <!-- Password -->
							      <div class="md-form">
							        <input type="password" id="materialLoginFormPassword" name="password"class="form-control">
							        <label for="materialLoginFormPassword">Password</label>
							      </div>
							      <div class="d-flex justify-content-around">
						        
						         <!-- Sign in button -->
    						     <button class="btn btn-outline-info btn-rounded btn-block my-4 waves-effect z-depth-0" type="submit" id="admin-signin-btn" onclick="adminLogin();">Sign in</button>
				</div>
			</div>
		</div>
		</div>



	</body>s
			<script type="text/javascript" src="JS/jQuery.js"></script>
			<script type="text/javascript" src="JS/admin.js"></script>
</html>