<?php
 session_start();
 session_destroy();
?>
<?php
include('header.php');
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Login</title>

		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" type="text/css" href="CSS/style.css">
		<link rel="stylesheet" type="text/css" href="CSS/responsive.css">
		<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	</head>
	<body>

	<div class="login-container">
				<div class="sidenav">
         <div class="login-main-text">
            <h2>User<br> Login Page</h2>
            <h3>Login or register from here to access.</h3>
         </div>
      </div>
      <div class="main">
         <div class="col-md-6 col-sm-12">
            <div class="login-form">
               <form method="POST">
                  <div class="form-group">
                     <label class="form-control-user-reg">Email</label>
                     <input type="text" class="form-control-user-reg" placeholder="Email" id="email">
                  </div>
                  <div class="form-group">
                     <label class="form-control-user-reg">Password</label>
                     <input type="password" class="form-control-user-reg" placeholder="Password" id="password">
                  </div>
                  <button type="button" class="btn btn-grey modify-btn" onclick="loginUser();">Login</button>
                  <button type="submit" class="btn btn-secondary modify-btn">Register</button>
               </form>
            </div>
         </div>
      </div>
			</div>

         <script type="text/javascript" src= "JS/link.js"></script>
      <script type="text/javascript" src= "JS/jQuery.js"></script>

	</body>
</html>