<?php 
	session_start();
	if (!isset($_SESSION['name'])) {
		?><script type="text/javascript">window.location.href='login.php';</script><?php
	}
session_destroy();
?>
<!DOCTYPE html>
<html>
	<head>
		<title> Index</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" type="text/css" href="CSS/style.css">
		<link rel="stylesheet" type="text/css" href="CSS/responsive.css">
		<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<div class="welcome-section col-md-12">
					<h2>Welcome to the Page <?php echo $_SESSION['name']; ?>. Here you can select the food you want to order along with quantity. Enjoy.</h2>
					<div class="main-menu-client pull-left">
							<h3>Signed in as: <?php echo $_SESSION['name']; ?></h3>
						</div>
						<button type="button" class="pull-right modify-btn">Your Order History</button>
						<button type="button" class="pull-right modify-btn" onclick="logOut();">Log Out</button>

					<div class="status-section pull-left">
								
							</div>
				</div>
			</div>
		</div>

		<div class="container-fluid">
			<div class="row">
				<div class="food-menu-section">
					<div class="col-md-3 pull-left hvr-grow">
							<img src="Images/pizza.jpg" class="modify-menu-food">
							<h3>Chicken Pizza (NRS.300)</h3>
							<div class="chicken-pizza col-md-12 pull-left">
								<form>
									 <label for="chicken-pizza">Choose a quantity:</label>
									  <select name="chicken-pizzapizza" id="chicken-pizza" onchange="getselectedvalue();">
									    <option value="0">0</option>
									    <option value="1">1</option>
									    <option value="2">2</option>
									    <option value="3">3</option>
									    <option value="4">4</option>
									    <option value="5">5</option>
									</select>
								</form>
							
						</div>
					</div>
					<div class="col-md-3 pull-left hvr-grow">
							<img src="Images/mushroom-pizza.jpg" class="modify-menu-food">
							<h3>Mushroom Pizza (NRS.230)</h3>
							<div class= "mushroom-pizza col-md-12 pull-left">
								<form>
									 <label for="mushroom-pizza">Choose a quantity:</label>
									  <select name="mushroom-pizza" id="mushroom-pizza" onchange="getselectedvalue();">
									    <option value="0">0</option>
									    <option value="1">1</option>
									    <option value="2">2</option>
									    <option value="3">3</option>
									    <option value="4">4</option>
									    <option value="5">5</option>
									</select>
								</form>
							
						</div>
					</div>

					<div class="col-md-3 pull-left hvr-grow">
							<img src="Images/cheese-pizza.jpg" class="modify-menu-food">
							<h3>Cheese Pizza (NRS.250)</h3>
							<div class= "cheese-pizza col-md-12 pull-left">
								<form>
									 <label for="cheese-pizza">Choose a quantity:</label>
									  <select name="cheese-pizza" id="cheese-pizza" onchange="getselectedvalue();">
									    <option value="0">0</option>
									    <option value="1">1</option>
									    <option value="2">2</option>
									    <option value="3">3</option>
									    <option value="4">4</option>
									    <option value="5">5</option>
									</select>
								</form>
							
						</div>
					</div>

						<div class="col-md-3 pull-left hvr-grow">
								<img src="Images/chicken-chilly.jpg" class="modify-menu-food">
								<h3>Chicken Chilly(NRS.280)</h3>
								<div class= "chicken-chilly col-md-12 pull-left">
									<form>
										 <label for="chicken-chilly">Choose a quantity:</label>
										  <select name="chicken-chilly" id="chicken-chilly" onchange="getselectedvalue();">
										    <option value="0">0</option>
										    <option value="1">1</option>
										    <option value="2">2</option>
										    <option value="3">3</option>
										    <option value="4">4</option>
										    <option value="5">5</option>
										</select>
									</form>
								
							</div>
						</div>

						<div class="col-md-3 pull-left hvr-grow">
								<img src="Images/buff-chilli.jpg" class="modify-menu-food">
								<h3>Buff Chilly(NRS.280)</h3>
								<div class= "buff-chilli col-md-12 pull-left">
									<form>
										 <label for="buff-chilli">Choose a quantity:</label>
										  <select name="buff-chilli" id="buff-chilli" onchange="getselectedvalue();">
										    <option value="0">0</option>
										    <option value="1">1</option>
										    <option value="2">2</option>
										    <option value="3">3</option>
										    <option value="4">4</option>
										    <option value="5">5</option>
										</select>
									</form>
								
							</div>
						</div>

						<div class="col-md-3 pull-left hvr-grow">
								<img src="Images/buff-chowmein.jpg" class="modify-menu-food">
								<h3>Buff Chowmein(NRS.150)</h3>
								<div class= "buff-chowmein col-md-12 pull-left">
									<form>
										 <label for="buff-chowmein">Choose a quantity:</label>
										  <select name="buff-chowmein" id="buff-chowmein" onchange="getselectedvalue();">
										    <option value="0">0</option>
										    <option value="1">1</option>
										    <option value="2">2</option>
										    <option value="3">3</option>
										    <option value="4">4</option>
										    <option value="5">5</option>
										</select>
									</form>
								
							</div>
						</div>

						<div class="col-md-3 pull-left hvr-grow">
								<img src="Images/Chicken-Chowmein.jpg" class="modify-menu-food">
								<h3>Chicken Chowmein(NRS.150)</h3>
								<div class= "Chicken-Chowmein col-md-12 pull-left">
									<form>
										 <label for="Chicken-Chowmein">Choose a quantity:</label>
										  <select name="Chicken-Chowmein" id="Chicken-Chowmein" onchange="getselectedvalue();">
										    <option value="0">0</option>
										    <option value="1">1</option>
										    <option value="2">2</option>
										    <option value="3">3</option>
										    <option value="4">4</option>
										    <option value="5">5</option>
										</select>
									</form>
								
							</div>
						</div>

						<div class="col-md-3 pull-left hvr-grow">
								<img src="Images/buff-momo.jpg" class="modify-menu-food">
								<h3>Buff MO:MO(NRS.150)</h3>
								<div class= "buff-momo col-md-12 pull-left">
									<form>
										 <label for="buff-momo">Choose a quantity:</label>
										  <select name="buff-momo" id="buff-momo" onchange="getselectedvalue();">
										    <option value="0">0</option>
										    <option value="1">1</option>
										    <option value="2">2</option>
										    <option value="3">3</option>
										    <option value="4">4</option>
										    <option value="5">5</option>
										</select>
									</form>
								
							</div>
						</div>

						<div class="col-md-3 pull-left hvr-grow">
								<img src="Images/chicken-momo.jpg" class="modify-menu-food">
								<h3>Chicken MO:MO(NRS.200)</h3>
								<div class= "chicken-momo col-md-12 pull-left">
									<form>
										 <label for="chicken-momo">Choose a quantity:</label>
										  <select name="chicken-momo" id="chicken-momo" onchange="getselectedvalue();">
										    <option value="0">0</option>
										    <option value="1">1</option>
										    <option value="2">2</option>
										    <option value="3">3</option>
										    <option value="4">4</option>
										    <option value="5">5</option>
										</select>
									</form>
								
							</div>
						</div>

						<div class="col-md-3 pull-left hvr-grow">
								<img src="Images/chicken-burger.jpg" class="modify-menu-food">
								<h3>Chicken Burger(NRS.200)</h3>
								<div class= "chicken-burger col-md-12 pull-left">
									<form>
										 <label for="chicken-burger">Choose a quantity:</label>
										  <select name="chicken-burger" id="chicken-burger" onchange="getselectedvalue();">
										    <option value="0">0</option>
										    <option value="1">1</option>
										    <option value="2">2</option>
										    <option value="3">3</option>
										    <option value="4">4</option>
										    <option value="5">5</option>
										</select>
									</form>
								
							</div>
						</div>
						<div class="col-md-3 pull-left hvr-grow">
								<img src="Images/Veg-burger.jpg" class="modify-menu-food">
								<h3>Veg Burger(NRS.200)</h3>
								<div class= "veg-burger col-md-12 pull-left">
									<form>
										 <label for="veg-burger">Choose a quantity:</label>
										  <select name="veg-burger" id="veg-burger" onchange="getselectedvalue();">
										    <option value="0">0</option>
										    <option value="1">1</option>
										    <option value="2">2</option>
										    <option value="3">3</option>
										    <option value="4">4</option>
										    <option value="5">5</option>
										</select>
									</form>
								
							</div>
						</div>

						<div class="col-md-3 pull-left hvr-grow">
								<img src="Images/fried-rice.jpg" class="modify-menu-food">
								<h3>Schezwan Egg Fried Rice(NRS.200)</h3>
								<div class= "fried-rice col-md-12 pull-left">
									<form>
										 <label for="fried-rice">Choose a quantity:</label>
										  <select name="fried-rice" id="fried-rice" onchange="getselectedvalue();">
										    <option value="0">0</option>
										    <option value="1">1</option>
										    <option value="2">2</option>
										    <option value="3">3</option>
										    <option value="4">4</option>
										    <option value="5">5</option>
										</select>
									</form>
								
							</div>
						</div>


						<div class="order-section col-md-12">
							<form>
								<label>Street Address 1:</label>
								<input type="text" id="street-address-one">
								<label>Street Address 2:</label>
								<input type="text" id="street-address-two">
								<label>Total:</label>
								<label class="total"></label>
								<input type="button" value="Order" class="modify-btn" onclick="orderFood();">

							</form>
							
						</div>
						
				
				</div>
				
					
			</div>
		</div>

	</body>
	 <script type="text/javascript" src= "JS/link.js"></script>
      <script type="text/javascript" src= "JS/jQuery.js"></script>
</html>




