
function userRegister(){
	 
	    var first_name = $('#first_name').val();
	    var last_name = $('#last_name').val();
	    var email = $('#email_address').val();
	    var first_password = $('#first_password').val();
	    var confirm_password = $('#confirm_password').val();
	    var mobile_no = $('#mobile_no').val();
	    var address_district = $('#address_district').val();
	    var address_area = $('#address_area').val();

	    var regEx = /\S+@\S+\.\S+/;

	    if (!first_name || !last_name || !email || !first_password || !confirm_password || !mobile_no || !address_district || !address_area) 
	    {
	     alert("Please Fill all the fields")
	    }
	    if(first_password!=confirm_password)
	    {
	    	alert("Password Does not match")
	    }

	    var validEmail = regEx.test(email);
	     if (!validEmail)
	      {
	      	alert("Pleae Enter Valid Email");
	     }
	     else{
		     	$.ajax({
							method: "POST",
							url: "logic.php",
							data: { first_name: first_name, last_name: last_name, email: email,
								first_password: first_password, mobile_no:mobile_no, address_district:address_district,
								address_area:address_area, request:'userRegister'}
					})
						.done(function( data ) {
							if(data){
								alert("Sucessfully registered.");
							}
							else{
								console.log(data);
							}
						});
	     }

}


function loginUser(){
	var email=$('#email').val();
	var password=$('#password').val();
	if (email=="" || password=="")
    {
				alert("Please enter your email and password");				
	}
				else{
						$.ajax({
							method: "POST",
							url: "logic.php",
							data: { email: email, password: password,request:'userLogin' }
						})
						.done(function( data ) {
							if(data){
								window.location.href="client-index.php";
								console.log(data);
							}
							else{
								alert("Couldn't Access");
							}
						});
				}
}


$( document ).ready(function() {
	$('.total').html("RS.0");

});



function getselectedvalue(){

var Chickenpizza= document.getElementById("chicken-pizza").options[document.getElementById("chicken-pizza").selectedIndex].text;
var Mushroompizza=document.getElementById("mushroom-pizza").options[document.getElementById("mushroom-pizza").selectedIndex].text;
var Cheesepizza=document.getElementById("cheese-pizza").options[document.getElementById("cheese-pizza").selectedIndex].text;
var Chickenchilly=document.getElementById("chicken-chilly").options[document.getElementById("chicken-chilly").selectedIndex].text;
var Buffchilly=document.getElementById("buff-chilli").options[document.getElementById("buff-chilli").selectedIndex].text;
var Buffchowmein=document.getElementById("buff-chowmein").options[document.getElementById("buff-chowmein").selectedIndex].text;
var Chickenchowmein=document.getElementById("Chicken-Chowmein").options[document.getElementById("Chicken-Chowmein").selectedIndex].text;
var Buffmomo=document.getElementById("buff-momo").options[document.getElementById("buff-momo").selectedIndex].text;
var Chickenmomo=document.getElementById("chicken-momo").options[document.getElementById("chicken-momo").selectedIndex].text;
var Chickenburger=document.getElementById("chicken-burger").options[document.getElementById("chicken-burger").selectedIndex].text;
var Vegburger=document.getElementById("veg-burger").options[document.getElementById("veg-burger").selectedIndex].text;
var Schezwanfriedrice= document.getElementById("fried-rice").options[document.getElementById("fried-rice").selectedIndex].text;
var Chickenpizzaprice=0;
var Mushroompizzaprice=0;
var Cheesepizzaprice=0;
var Chickenchillyprice=0;
var Buffchillyprice=0;
var Buffchowmeinprice=0;
var Chickenchowmeinprice=0;
var Buffmomoprice=0;
var Chickenmomoprice=0;
var Chickenburgerprice=0;
var Vegburgerprice=0;
var Schezwanfriedriceprice=0;

	if(Chickenpizza!= 'Choose'){
		 Chickenpizzaprice=300*Chickenpizza;
	}
	else{
			Chickenpizzaprice=0;
		}
	if(Mushroompizza!= 'Choose'){
		 Mushroompizzaprice=230*Mushroompizza;
	}
	else{							
			Mushroompizzaprice=0;
		}
	if(Cheesepizza!= 'Choose'){
		 Cheesepizzaprice=250*Cheesepizza;
	}
	else{
			Cheesepizzaprice=0;
		}
	if(Chickenchilly!= 'Choose'){
		Chickenchillyprice=280*Chickenchilly;
	}
	else{
			Chickenchillyprice=0;
		}
	if(Buffchilly!= 'Choose'){
		 Buffchillyprice=280*Buffchilly;
	}
	else{
			Buffchillyprice=0;
		}
	if( Buffchowmein!= 'Choose'){
		 Buffchowmeinprice=150* Buffchowmein;
	}
	else{
			 Buffchowmeinprice=0;
		}
	if(Chickenchowmein!= 'Choose'){
		 Chickenchowmeinprice=150*Chickenchowmein;
	}
	else{
			Chickenchowmeinprice=0;
		}
	if(Buffmomo!= 'Choose'){
		 Buffmomoprice=150*Buffmomo;
	}
	else{
			Buffmomoprice=0;
		}
	if(Chickenmomo!= 'Choose'){
		 Chickenmomoprice=150*Chickenmomo;
	}
	else{
			Chickenmomo=0;
		}

	if(Chickenburger!= 'Choose'){
			Chickenburgerprice=200*Chickenburger;		
		}
		else{
				Chickenburgerprice=0;
			}

	if(Vegburger!= 'Choose'){
			 Vegburgerprice=200*Vegburger;
		}
		else{
				Vegburgerprice=0;
			}

	if(Schezwanfriedrice!= 'Choose'){
			Schezwanfriedriceprice=200*Schezwanfriedrice;
		}
		else{
				Schezwanfriedriceprice=0;
			}

		var totalsum=Chickenpizzaprice+Mushroompizzaprice+Cheesepizzaprice+Chickenchillyprice+Buffchillyprice+Buffchowmeinprice+Chickenchowmeinprice+
		Buffmomoprice+Chickenmomoprice+Chickenburgerprice+Vegburgerprice+Schezwanfriedriceprice;

$('.total').html("RS."+totalsum);
console.log(totalsum);

	
}
function orderFood(){
	var customer_id=$('#loggedinCustomerid').val();

	var Streetaddressone = $('#street-address-one').val();
	var Streetaddresstwo = $('#street-address-two').val();

var Chickenpizza= document.getElementById("chicken-pizza").options[document.getElementById("chicken-pizza").selectedIndex].text;
var Mushroompizza=document.getElementById("mushroom-pizza").options[document.getElementById("mushroom-pizza").selectedIndex].text;
var Cheesepizza=document.getElementById("cheese-pizza").options[document.getElementById("cheese-pizza").selectedIndex].text;
var Chickenchilly=document.getElementById("chicken-chilly").options[document.getElementById("chicken-chilly").selectedIndex].text;
var Buffchilly=document.getElementById("buff-chilli").options[document.getElementById("buff-chilli").selectedIndex].text;
var Buffchowmein=document.getElementById("buff-chowmein").options[document.getElementById("buff-chowmein").selectedIndex].text;
var Chickenchowmein=document.getElementById("Chicken-Chowmein").options[document.getElementById("Chicken-Chowmein").selectedIndex].text;
var Buffmomo=document.getElementById("buff-momo").options[document.getElementById("buff-momo").selectedIndex].text;
var Chickenmomo=document.getElementById("chicken-momo").options[document.getElementById("chicken-momo").selectedIndex].text;
var Chickenburger=document.getElementById("chicken-burger").options[document.getElementById("chicken-burger").selectedIndex].text;
var Vegburger=document.getElementById("veg-burger").options[document.getElementById("veg-burger").selectedIndex].text;
var Schezwanfriedrice= document.getElementById("fried-rice").options[document.getElementById("fried-rice").selectedIndex].text;
var totalsum=(300*Chickenpizza)+(230*Mushroompizza)+(250*Cheesepizza)+(280*Chickenchilly)+(280*Buffchilly)+
								(150*Buffchowmein)+(150*Chickenchowmein)+(150*Buffmomo)+(200*Chickenmomo)+(200*Chickenburger)+(200*Vegburger)+(200*Schezwanfriedrice);
console.log("ttt"+totalsum);

		if(totalsum==0){
			alert("Please Choose a your order");
		}
		else{
			if(Streetaddressone=="" && Streetaddresstwo==""){
				alert("Please fill up the both street adress so it will easy for us to Deleiver your order.");
			}

			else{

					$.ajax({
								method: "POST",
								url: "logic.php",
								data: { customer_id:customer_id,Streetaddressone: Streetaddressone, Streetaddresstwo: Streetaddresstwo, Chickenpizza:Chickenpizza,
									Mushroompizza: Mushroompizza, Cheesepizza: Cheesepizza, Chickenchilly: Chickenchilly, Buffchilly: Buffchilly,
									Buffchowmein: Buffchowmein, Chickenchowmein: Chickenchowmein, Buffmomo: Buffmomo, Chickenmomo: Chickenmomo,
									Chickenburger: Chickenburger, Vegburger: Vegburger, Schezwanfriedrice: Schezwanfriedrice, Total:totalsum, request:'foodOrder'}
							})
							.done(function( data ) {
								if(data.length > 0){
									alert(data.trim());
								}
								else{
									console.log(data);
								}
							});

			}

		}

	

}

/*$( document ).ready(function() {
	showStatus();

	});

function showStatus(){
	$.ajax('show-status.php').done(function(res){

					console.log(res);
					data=JSON.parse(res);
					var value= data.Status;
					if (value==1){
						$('.status-section').html("Status:Your Order is getting Ready");
					}
					if (value==2){
						$('.status-section').html("Status:Your Order is getting Delivered");
					}
					else{
						$('.status-section').html("Status:No Order Yet");

					}
					console.log(data.Status);
				});
};*/


Slider=setInterval(myFunction,3000);
function myFunction(){
	var current_index=0;
	$('.modify-editedmap').each(function(el){
		if($(this).hasClass('active')){
			 current_index=$(this).index();
			$(this).removeClass('active');
		}
	})
	if (current_index == 1) {
		$('.modify-editedmap').eq(0).addClass('active');
	}
	else{ 
		$('.modify-editedmap').eq(current_index+1).addClass('active');
	}
	
}

function logOut(){
	window.location.href="login.php";
}


function feedback(){
	var user_name=$('#user_name').val();
	var user_email=$('#user_email').val();
	var user_mob=$('#user_mob').val();
	var user_msg=$('#user_msg').val();

	if (!user_name || !user_email || !user_mob || !user_msg) {

		alert("All Fields are Necessary");
	}
	else{

			$.ajax({
							method: "POST",
							url: "logic.php",
							data: { user_name: user_name, user_email: user_email, user_mob:user_mob, user_msg:user_msg, request:'feedback'}
					})
						.done(function( data ) {
							if(data){
								alert("Your Feedback is submitted");
							}
							else{
								console.log(data);
							}
						});

	}


}


	function orderHistory(){
	var customer_id=$('#loggedinCustomerid').val();

		$.ajax({
							method: "POST",
							url: "logic.php",
							data: { customer_id: customer_id, request:'orderHistory'}
					})
						.done(function( res, textStatus, jqXHR ) {
							console.log(res);
							if(res!=null){
								data = JSON.parse(res);
								console.log(res);

								var table = "<table border='2px' id='mdf-table'><thead><tr><th>Address1</th><th>Address2</th><th>Chickenpizza</th><th>Mushroompizza</th><th>Cheesepizza</th><th>Chickenchilly</th><th>Buffchilly</th><th>Buffchowmein</th><th>Chickenchowmein</th><th>Buffmomo</th><th>Chickenmomo</th><th>Chickenburger</th><th>Vegburger</th><th>Schezwanfriedrice</th><th>Total</th><th>Order ID</th><th>Time</th></tr></thead><tbody></tbody></table>";
								

								var wholeData = JSON.parse(res);
								data = wholeData[0]; //the items and quantity ordered
								var orderDetals = wholeData[1]; //the details like order time, customer etc.
								var tableBody;
								for(var i=0;  i < data.length; i++){
									var eachRow = '<tr><td>'+data[i].address_One+'</td><td>'+data[i].address_Two+'</td><td>'+data[i].Chickenpizza+'</td><td>'+data[i].Mushroompizza+'</td><td>'+data[i].Cheesepizza+'</td><td>'+data[i].Chickenchilly+'</td><td>'+data[i].Buffchilly+'</td><td>'+data[i].Buffchowmein+'</td><td>'+data[i].Chickenchowmein+'</td><td>'+data[i].Buffmomo+'</td><td>'+data[i].Chickenmomo+'</td><td>'+data[i].Chickenburger+'</td><td>'+data[i].Vegburger+'</td><td>'+data[i].Schezwanfriedrice+'</td><td>'+data[i].Total+'</td><td>'+data[i].order_id+'</td><td>'+orderDetals[i].time_stamp+'</td></tr>';
									tableBody = tableBody+eachRow;
								}
								// var mainTable = "<table border='2px' id='mdf-table'>"+tableHeader+tableBody+"</table>";

								//var table='<table border="2px"><tr><td>'+data.address_One+'</td><td>'+data.address_Two+'</td><td>'+data.Chickenpizza+'</td><td>'+data.Mushroompizza+'</td><td>'+data.Cheesepizza+'</td><td>'+data.Chickenchilly+'</td><td>'+data.Buffchilly+'</td><td>'+data.Buffchowmein+'</td><td>'+data.Chickenchowmein+'</td><td>'+data.Buffmomo+'</td><td>'+data.Chickenmomo+'</td><td>'+data.Chickenburger+'</td><td>'+data.Vegburger+'</td><td>'+data.Schezwanfriedrice+'</td><td>'+data.Total+'</td><td>'+data.order_id+'</td><td>'+data.ordered_by+'</td></tr></table>';
								$('.food-menu-section').hide();
								$('.order-history').show();
								$('.order-history').empty();
								$('.order-history').append(table);
							
								$('#mdf-table tbody').empty();
								$('#mdf-table tbody').append(tableBody);

							}
							else("Error");
							
						});
	}
	function showMenu(){
		$('.order-history').hide();
		$('.food-menu-section').show();

	}





