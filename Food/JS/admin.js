function adminLogin(){
	var email=$('#materialLoginFormEmail').val();
	var password=$('#materialLoginFormPassword').val();
	if (email=="" || password=="")
    {
				alert("Please enter your email and password");
	}
				else{
						$.ajax({
							method: "POST",
							url: "admin-logic.php",
							data: { email: email, password: password,request:'adminLogin' }
						})
						.done(function( data ) {
							if(data>0){
								window.location.href="admin-index.php";
							}
							else{
								console.log(data);
							}
						});
				}

}

$( document ).ready(function() {
	var pageId = $('#pageId').val();
	if(pageId=='ordered'){
		showOrderedItems();
	}
	else if (pageId=='order-delivering') {
		showDelivering();
	}
	else if (pageId=='order-delivered') {
		showDelivered();
	}
	else if (pageId=='search-customers') {
		searchCustomer('all_customers');
	}
	else{
		//
	}
});


function showOrderedItems(){
	// var tableHeader = "<tr><th>Address1</th><th>Address2</th><th>Chickenpizza</th><th>Mushroompizza</th><th>Cheesepizza</th><th>Chickenchilly</th><th>Buffchilly</th><th>Buffchowmein</th><th>Chickenchowmein</th><th>Buffmomo</th><th>Chickenmomo</th><th>Chickenburger</th><th>Vegburger</th><th>Schezwanfriedrice</th><th>Total</th><th>Order ID</th><th>Ordered By</th></tr>";
	$.ajax('show-orderd.php').done(function(res){
					console.log(res);
					var wholeData = JSON.parse(res);
					data = wholeData[0]; //the items and quantity ordered
					var orderDetals = wholeData[1]; //the details like order time, customer etc.
					var tableBody;
					for(var i=0;  i < data.length; i++){
						var eachRow = '<tr><td>'+data[i].address_One+'</td><td>'+data[i].address_Two+'</td><td>'+data[i].Chickenpizza+'</td><td>'+data[i].Mushroompizza+'</td><td>'+data[i].Cheesepizza+'</td><td>'+data[i].Chickenchilly+'</td><td>'+data[i].Buffchilly+'</td><td>'+data[i].Buffchowmein+'</td><td>'+data[i].Chickenchowmein+'</td><td>'+data[i].Buffmomo+'</td><td>'+data[i].Chickenmomo+'</td><td>'+data[i].Chickenburger+'</td><td>'+data[i].Vegburger+'</td><td>'+data[i].Schezwanfriedrice+'</td><td>'+data[i].Total+'</td><td>'+data[i].order_id+'</td><td>'+orderDetals[i].customer_id+'</td></tr>';
						tableBody = tableBody+eachRow;
					}
					// var mainTable = "<table border='2px' id='mdf-table'>"+tableHeader+tableBody+"</table>";

					//var table='<table border="2px"><tr><td>'+data.address_One+'</td><td>'+data.address_Two+'</td><td>'+data.Chickenpizza+'</td><td>'+data.Mushroompizza+'</td><td>'+data.Cheesepizza+'</td><td>'+data.Chickenchilly+'</td><td>'+data.Buffchilly+'</td><td>'+data.Buffchowmein+'</td><td>'+data.Chickenchowmein+'</td><td>'+data.Buffmomo+'</td><td>'+data.Chickenmomo+'</td><td>'+data.Chickenburger+'</td><td>'+data.Vegburger+'</td><td>'+data.Schezwanfriedrice+'</td><td>'+data.Total+'</td><td>'+data.order_id+'</td><td>'+data.ordered_by+'</td></tr></table>';
					$('#mdf-table').children('tbody').empty();
					$('#mdf-table tbody').append(tableBody);
				});
};



	//var eachRow = '<tr><td>'+data[i].address_One+'</td><td>'+data[i].address_Two+'</td><td>'+data[i].Chickenpizza+'</td><td>'+data[i].Mushroompizza+'</td><td>'+data[i].Cheesepizza+'</td><td>'+data[i].Chickenchilly+'</td><td>'+data[i].Buffchilly+'</td><td>'+data[i].Buffchowmein+'</td><td>'+data[i].Chickenchowmein+'</td><td>'+data[i].Buffmomo+'</td><td>'+data[i].Chickenmomo+'</td><td>'+data[i].Chickenburger+'</td><td>'+data[i].Vegburger+'</td><td>'+data[i].Schezwanfriedrice+'</td><td>'+data[i].	Total+'</td><td>'+data[i].order_id+'</td></tr>'
						//tableBody = tableBody+eachRow;



function toDelivering(){
	var order_id = $('#Delivering').val();
	if (order_id == null || order_id == ''){
		alert('Field Empty');
	}
	else{
		$.ajax({
				      method: "POST",
							url: "admin-logic.php",
							data: {order_id:order_id,request:'toDelivering'}
						})
						.done(function(data, textStatus, jqXHR){
							console.log(data);
							console.log("ajax_status: " + textStatus + " resonse-data.lenth: " + data.trim().length);
							if(textStatus === "success"){
								showOrderedItems();
								if(data.trim() === "delivery_status changed to delivering"){
									//alert("gyurme");
									//showOrderedItems();
								}
							}
						})
						.fail(function(data, textStatus, jqXHR){
							alert("fail: " + textStatus);
							showOrderedItems();
						});
	}

}

function showDelivering(){
	// var tableHeader = "<tr><th>Address1</th><th>Address2</th><th>Chickenpizza</th><th>Mushroompizza</th><th>Cheesepizza</th><th>Chickenchilly</th><th>Buffchilly</th><th>Buffchowmein</th><th>Chickenchowmein</th><th>Buffmomo</th><th>Chickenmomo</th><th>Chickenburger</th><th>Vegburger</th><th>Schezwanfriedrice</th><th>Total</th><th>Order ID</th><th>Ordered By</th></tr>";
	$.ajax('show-delivering.php').done(function(res){
		var wholeData = JSON.parse(res);
		data = wholeData[0]; //the items and quantity ordered
		var orderDetals = wholeData[1]; //the details like order time, customer etc.
		var tableBody;
		for(var i=0;  i < data.length; i++){
			var eachRow = '<tr><td>'+data[i].address_One+'</td><td>'+data[i].address_Two+'</td><td>'+data[i].Chickenpizza+'</td><td>'+data[i].Mushroompizza+'</td><td>'+data[i].Cheesepizza+'</td><td>'+data[i].Chickenchilly+'</td><td>'+data[i].Buffchilly+'</td><td>'+data[i].Buffchowmein+'</td><td>'+data[i].Chickenchowmein+'</td><td>'+data[i].Buffmomo+'</td><td>'+data[i].Chickenmomo+'</td><td>'+data[i].Chickenburger+'</td><td>'+data[i].Vegburger+'</td><td>'+data[i].Schezwanfriedrice+'</td><td>'+data[i].Total+'</td><td>'+data[i].order_id+'</td><td>'+orderDetals[i].customer_id+'</td></tr>';
						tableBody = tableBody+eachRow;

					}
					// var mainTable = "<table border='2px'id='mdf-table-Delivering'>"+tableHeader+tableBody+"</table>";
					// 	$('.delivering-items-section').html(mainTable);
					$('#mdf-table-Delivering').children('tbody').empty();
					$('#mdf-table-Delivering tbody').append(tableBody);
				});

}

function toDelivered(){
	var order_id=$('#Delivered').val();
	if (order_id==""){
		alert('Field Empty');
	}
	else{
		$.ajax({
				        	method: "POST",
							url: "admin-logic.php",
							data: { order_id: order_id,request:'toDelivered' }
						})
						.done(function(data, textStatus, jqXHR){
							if(textStatus === "success"){
								alert(data.trim());
								showDelivering();
								if(data.trim() === "delivery_status changed to delivered"){
									alert("gyurme");
									//showOrderedItems();
								}
							}
						})
						.fail(function(data){
							alert(data);
							showOrderedItems();
						});
	}
}


function showDelivered(){
	var tableHeader = "<tr><th>Address1</th><th>Address2</th><th>Chickenpizza</th><th>Mushroompizza</th><th>Cheesepizza</th><th>Chickenchilly</th><th>Buffchilly</th><th>Buffchowmein</th><th>Chickenchowmein</th><th>Buffmomo</th><th>Chickenmomo</th><th>Chickenburger</th><th>Vegburger</th><th>Schezwanfriedrice</th><th>Total</th><th>Order ID</th><th>Ordered By</th></tr>";
	$.ajax('show-delivered.php').done(function(res){
		var wholeData = JSON.parse(res);
		data = wholeData[0]; //the items and quantity ordered
		var orderDetals = wholeData[1]; //the details like order time, customer etc.
		var tableBody;
		for(var i=0;  i < data.length; i++){
			var eachRow = '<tr><td>'+data[i].address_One+'</td><td>'+data[i].address_Two+'</td><td>'+data[i].Chickenpizza+'</td><td>'+data[i].Mushroompizza+'</td><td>'+data[i].Cheesepizza+'</td><td>'+data[i].Chickenchilly+'</td><td>'+data[i].Buffchilly+'</td><td>'+data[i].Buffchowmein+'</td><td>'+data[i].Chickenchowmein+'</td><td>'+data[i].Buffmomo+'</td><td>'+data[i].Chickenmomo+'</td><td>'+data[i].Chickenburger+'</td><td>'+data[i].Vegburger+'</td><td>'+data[i].Schezwanfriedrice+'</td><td>'+data[i].Total+'</td><td>'+data[i].order_id+'</td><td>'+orderDetals[i].customer_id+'</td></tr>';
						tableBody = tableBody+eachRow;

					}
					var mainTable = "<table border='2px'id='mdf-table-Delivering'>"+tableHeader+tableBody+"</table>";
						$('.delivered-items-section').html(mainTable);
				});

}



function searchCustomer(Customer_id){
	//var Customer_id=$('#Customer_id').val();
	if (Customer_id!=null){

						$.ajax({
							method: "POST",
							url: "searchcustomerid.php",
							data: { Customer_id: Customer_id, request:'searchCustomer' }
						})
						.done(function(res) {
							console.log(res);
							if(res!=null){
								data = JSON.parse(res);
								console.log(data.length);

								var tableHeader = "<tr><th>First Name</th><th>Last Name</th><th>Email</th><th>Contact</th><th>Address one</th><th>Address Two</th></tr>";

								var tableBody;
								for(var i=0;  i < data.length; i++){
									var eachRow = '<tr><td>'+data[i].first_name+'</td><td>'+data[i].last_name+'</td><td>'+data[i].email+'</td><td>'+data[i].mobile_no+'</td><td>'+data[i].address_district+'</td><td>'+data[i].address_area+'</td></tr>'
									tableBody = tableBody+eachRow;
								}
								var mainTable = "<table border='2px' id=''>"+tableHeader+tableBody+"</table>";
								$('.customer-info').html(mainTable);




							}
							else{
								console.log('error');
							}
						});
	}

	else{
		console.log("error");
	}
}

function toBlockCustomer(){

	var Customer_id=$('#Customer_id').val();
	if (Customer_id!=null) {
			$.ajax({
						method: "POST",
						url: "admin-logic.php",
						data: { Customer_id: Customer_id, request:'blockCustomer' }
					})

						.done(function(res) {
							if (res!=null) {
								alert("Client Blocked Sucessfully");
							}
							else{
								alert("Couldn't Block Client");
							}
						});

	}
	else{

		alert('Please Enter User ID to block the Customer');
	}

}







/*function searchallCustomer(){
	var tableHeader = "<tr><th>First Name</th><th>Last Name</th><th>Email</th><th>Contact</th><th>Address one</th><th>Address Two</th></tr>";

	var Customer_id=$('#Customer_id').val();
	if (Customer_id!=null){

						$.ajax({
							method: "POST",
							url: "showallCustomer.php",
							data: { request:'searchallCustomer' }
						})

						.done(function( res ) {
							console.log(res);

							data=JSON.parse(res);


							if(data>0){


									var tableBody;
									for (var i=0;  i < data.length; i++) {
										var eachRow = '<tr><td>'+data[i].first_name+'</td><td>'+data[i].last_name+'</td><td>'+data[i].email+'</td><td>'+data[i].mobile_no+'</td><td>'+data[i].address_district+'</td><td>'+data[i].address_area+'</td></tr>'
										tableBody = tableBody+eachRow;
									}
									var mainTable = "<table border='2px' id='mdf-table-Delivered'>"+tableHeader+tableBody+"</table>";
									$('.customer-info').html(mainTable);




							}
							else{
								console.log(data);
							}
						});
	}

	else{
		console.log("error");
	}
}*/





//$.ajax({
//							method: "POST",
//							url: "admin-logic.php",
//							data: { Customer_id: Customer_id, request:'searchCustomer' }
//						})


/*function showStatement(){
	$.ajax('showTransaction.php').done(function(res){
		console.log(res);
		data = JSON.parse(res);
		console.log(data.sentby);
		var table='<table border="1px"><tr><td>sent by:</td><td>'+data.sentby+'</td></tr><tr><td>receipt by:</td><td>'+data.receiptby+'</td></tr><tr><td>Amount:</td><td>' data.amount+'</td></tr><tr><td>Sender Final Amount:</td><td>'+data.sender_final_amt+'</td></tr></table>';
		$('.statement-form').html(table);
	})
}*/







//		var tableHeader = "<tr><th>Address1</th><th>Address2</th><th>Chickenpizza</th><th>Mushroompizza</th><th>Cheesepizza</th><th>Chickenchilly</th><th>Buffchilly</th><th>Buffchowmein</th><th>Chickenchowmein</th><th>Buffmomo</th><th>Chickenmomo</th><th>Chickenburger</th><th>Vegburger</th><th>Schezwanfriedrice</th><th>Total</th><th>Order ID</th></tr>";
	//$.ajax('show-delivering.php').done(function(res){

				//	data=JSON.parse(res);
				//	var tableBody;
				//	for(var i=0;  i < data.length; i++){
				//		var eachRow = '<tr><td>'+data[i].address_One+'</td><td>'+data[i].address_Two+'</td><td>'+data[i].Chickenpizza+'</td><td>'+data[i].Mushroompizza+'</td><td>'+data[i].Cheesepizza+'</td><td>'+data[i].Chickenchilly+'</td><td>'+data[i].Buffchilly+'</td><td>'+data[i].Buffchowmein+'</td><td>'+data[i].Chickenchowmein+'</td><td>'+data[i].Buffmomo+'</td><td>'+data[i].Chickenmomo+'</td><td>'+data[i].Chickenburger+'</td><td>'+data[i].Vegburger+'</td><td>'+data[i].Schezwanfriedrice+'</td><td>'+data[i].	Total+'</td><td>'+data[i].order_id+'</td></tr>'
				//		tableBody = tableBody+eachRow;
				//	}
				//	var mainTable = "<table border='2px'>"+tableHeader+tableBody+"</table>";
					//	$('.ordered-items-section').html(mainTable);
				//});

//}
