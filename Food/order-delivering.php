<!DOCTYPE html>
<html>
	<head>
		<title> Index</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" type="text/css" href="CSS/style.css">
		<link rel="stylesheet" type="text/css" href="CSS/responsive.css">
		<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	</head>
	<body>
		<div class="admin-menu-section">
					<ul class="main-menu">
							<a href="admin-index.php"><li onclick="showOrderedItems();">Ordered Items</li></a>
							<a href="order-delivering.php"><li onclick="showDelivering();">Delivering  Items</li></a>
							<a href="order-delivered.php"><li onclick="showDelivered();">Delivered Items</li></a>
							<a href="search-customer.php"><li>Search Customer</li></a>


						</ul>
						<div class="control-panel-section">
							<form class="modify-admin-panel-form">
								<input type="text" name="" id="Delivered">
								<button onclick="toDelivered();" class="modify-btn">Delivered.</button>
								<input type="hidden" id="pageId" name="pageId" value="order-delivering">
							</form>

						</div>
		</div>

		<div class="delivering-items-section">
			<table border='2px'id='mdf-table-Delivering'>
				<thead>
					<tr>
						<th>Address1</th><th>Address2</th><th>Chickenpizza</th><th>Mushroompizza</th><th>Cheesepizza</th><th>Chickenchilly</th>
						<th>Buffchilly</th><th>Buffchowmein</th><th>Chickenchowmein</th><th>Buffmomo</th><th>Chickenmomo</th><th>Chickenburger</th>
						<th>Vegburger</th><th>Schezwanfriedrice</th><th>Total</th><th>Order ID</th><th>Ordered By</th>
					</tr>
				</thead>
				<tbody>

				</tbody>
			</table>
		</div>




	</body>
	 <script type="text/javascript" src= "JS/admin.js"></script>
      <!-- <script type="text/javascript" src= "JS/jQuery.js"></script> -->
</html>
