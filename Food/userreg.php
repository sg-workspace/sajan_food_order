<?php
include('header.php');
?>
<!DOCTYPE html>
<html>
	<head>
		<title>User Registeration</title>

		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" type="text/css" href="CSS/style.css">
		<link rel="stylesheet" type="text/css" href="CSS/responsive.css">
		<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	</head>
	<body>
			<div class="user-reg-container">
				<div class="pizza-image-container">
				<img src="Images/pizza.jpg" class="modify-pizza-image">

				</div>

				<form  class="user-reg-form-group" id="userRegForm" method="POST">
					<label class="form-control-user-reg" >First Name</label><br>
					<input type="text" class="form-control-user-reg" id="first_name"><br>
					<label class="form-control-user-reg">Last Name</label><br>
				    <input type="text" class="form-control-user-reg" id="last_name"><br>
					<label class="form-control-user-reg">Email Address</label><br>
					<input type="text" class="form-control-user-reg" id="email_address"><br>
					<label class="form-control-user-reg">Password</label><br>
					<input type="password" class="form-control-user-reg" id="first_password"><br>
					<label class="form-control-user-reg">Confirm Password</label><br>
					<input type="password" class="form-control-user-reg" id="confirm_password"><br>
					<label class="form-control-user-reg">Mobile No.</label><br>
					<input type="Number" class="form-control-user-reg" id="mobile_no"><br>
					<label class="form-control-user-reg">Address (Area Location)</label><br>
					<input type="text" class="form-control-user-reg" id="address_district"><br>
					<label class="form-control-user-reg">Address(Street Location)</label><br>
					<input type="text" class="form-control-user-reg" id="address_area"><br><br>
					<input type="button" value="Regsiter" class="modify-btn" onclick="userRegister();">
				</form>
				
			</div>
			<script type="text/javascript" src= "JS/link.js"></script>
		<script type="text/javascript" src= "JS/jQuery.js"></script>
	</body>
</html>